package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.IProjectController;
import ru.t1.mayornikov.tm.api.IProjectService;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        int index = 1;
        for (final Project project: projectService.findAll()) System.out.println(index++ + ". " + project.getName());
        if (index == 1) System.out.println("No one project found...");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextline();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextline();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[DONE]");
    }

}