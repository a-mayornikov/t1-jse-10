package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.ITaskController;
import ru.t1.mayornikov.tm.api.ITaskService;
import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        int index = 1;
        for(final Task task: taskService.findAll()) System.out.println(index++ + ". " + task.getName());
        if (index == 1) System.out.println("No one task found... ");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextline();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextline();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[DONE]");
    }

}