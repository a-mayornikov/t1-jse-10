package ru.t1.mayornikov.tm.api;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
