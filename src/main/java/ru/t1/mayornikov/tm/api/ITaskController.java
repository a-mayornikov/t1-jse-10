package ru.t1.mayornikov.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}