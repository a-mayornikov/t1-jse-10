package ru.t1.mayornikov.tm.api;

import ru.t1.mayornikov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task add(Task task);

    void clear();

    List<Task> findAll();

}