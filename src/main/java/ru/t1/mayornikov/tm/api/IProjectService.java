package ru.t1.mayornikov.tm.api;

import ru.t1.mayornikov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
