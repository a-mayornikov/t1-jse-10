package ru.t1.mayornikov.tm.util;

import static ru.t1.mayornikov.tm.constant.FormatConst.*;

public interface FormatUtil {

    static String formatBytes(final long bytes) {
        if (bytes <= BYTES_K && bytes >= 0) return toFormat(bytes, 1, "B");
        else if (bytes <= BYTES_M) return toFormat(bytes, BYTES_K, "KB");
        else if (bytes <= BYTES_G) return toFormat(bytes, BYTES_M, "MB");
        else if (bytes <= BYTES_T) return toFormat(bytes, BYTES_G, "GB");
        else if (bytes <= BYTES_P) return toFormat(bytes, BYTES_T,"TB");
        else return toFormat(bytes, BYTES_P, "PB");
    }

    static String toFormat(long bytes, double bytesScale, String bytesAttribute) {
        return DECIMAL_FORMAT.format(bytes / bytesScale) + " " + bytesAttribute;
    }

}