package ru.t1.mayornikov.tm.constant;

import java.text.DecimalFormat;

public final class FormatConst {

    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    public static final double BYTES_K = 1024;

    public static final double BYTES_M = BYTES_K * 1024;

    public static final double BYTES_G = BYTES_M * 1024;

    public static final double BYTES_T = BYTES_G * 1024;

    public static final double BYTES_P = BYTES_T * 1024;

    private FormatConst(){
    }

}